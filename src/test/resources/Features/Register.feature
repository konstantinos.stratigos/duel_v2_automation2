Feature: DUELQA_BAH_BATELCO_02_SDP (https://staging-duel-bah-batelco_02.velti.com:493/)

  Background: 
    Given browser is open
    And user clicks on menu language
    When user clicks on main menu

  Scenario Outline: Check Register is successful
    And user clicks on Register
    And user writes phone number <password>
    And user clicks on Continue
    And user write code <code>
    And user won <points>
    And seems the welcome with user
    Then user is navigated to the home page
    And delete user with <password>

    Examples: 
      | password    | code | points |
      | 97356567694 | 1334 |  100   |
     

  Scenario Outline: Check Login is successful
    And user clicks on Login
    When user writes <mobilenumberlogin> and <passwordlogin>
    Then user is navigated to the home page
    
    Examples: 
      | mobilenumberlogin | passwordlogin |
      |    97356564879    |     1234      |
