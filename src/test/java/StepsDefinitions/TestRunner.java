package StepsDefinitions;


import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/Features/Register.feature", glue={"StepsDefinitions"},


plugin = { "pretty",  "junit:target/JUnitReports/report.xml",
		"json:target/cucumber.json",
		"html:target/surefire-reports/emailable-report.html"}

		)

public class TestRunner {

}
