package StepsDefinitions;



import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;

//import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.codejava.JavaMySQLTest;
import pages.Browsers;
import pages.Login_page;
import pages.Register_page;




public class Register {
	
	WebDriver driver=null;
	Register_page Register;
	Browsers chrome;
	JavaMySQLTest data;
	Login_page Login;

	private By btn_Login=By.xpath("//*[@id=\"nav_dropdown\"]/ul/li[4]/a");
	@Given("browser is open")
	
	public void browser_is_open() throws InterruptedException{
		
		
		
		System.out.println("====I am inside RegisterSteps_DUEL Staging Environment====");
		System.out.println("Inside Step-Browser is open");
			//chrome = new Browsers (driver);
			//chrome.chrome();
			
			
			Thread.sleep(4000);
//chrome drivers
		String projectPath =System.getProperty("user.dir");
		System.out.println("Project path is : "+ projectPath);
		System.setProperty("webdriver.chrome.driver",projectPath +"/src/test/resources/drivers/chromedriver.exe");
	    driver = new ChromeDriver();
		driver.get("https://staging-duel-bah-batelco_02.velti.com:493/");		
		
//firefox drivers
//		String projectPath =System.getProperty("user.dir");
//		System.out.println("Project path is : "+ projectPath);
//		System.setProperty("webdriver.gecko.driver",projectPath +"/src/test/resources/drivers/geckodriver.exe");
//		WebDriver driver = new FirefoxDriver();
//		driver.get("http://staging-duel-bah-batelco_02.velti.com:493/");
		//kostastest = new SQLConnector(driver);
		
	}	

	@And("user clicks on menu language")
	public void user_clicks_on_menu_language() throws InterruptedException{
		
		Thread.sleep(4000);
		driver.findElement(By.xpath("/html/body/div[2]/div/header/div/nav/ul")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("/html/body/div[2]/div/header/div/nav/ul/li/ul/li[2]/a")).click();		
	}

	@When("user clicks on main menu")
	public void user_clicks_on_main_menu() throws InterruptedException{
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id=\"app\"]/div/header/div/nav/button")).click();
		Thread.sleep(4000);
	}

	@And("user clicks on Register")
	public void user_clicks_on_register() throws InterruptedException{
		
		driver.findElement(By.xpath("//*[@id=\"nav_dropdown\"]/ul/li[5]/a")).click();
		Thread.sleep(4000);
	}

	@And("user writes phone number (.*)$")
	public void user_writes_phone_number(String password)  throws InterruptedException{
		Thread.sleep(4000);
	   Register = new Register_page(driver);
	   Register.insertPassword(password);
		
	}

	@And("user clicks on Continue")
	public void user_clicks_on_continue() throws InterruptedException{
		Thread.sleep(4000);
		Register.click_continue();
		
//		Thread.sleep(4000);
//		WebElement strValue = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/form/div[1]/span"));
//		String strExpected = "Oops. It appears that this number is already registered. To continue playing simply login";
//		String strActual = strValue.getText();
//		System.out.println(strActual);
//				
//		if (strExpected.equals(strActual )) {
//			//System.out.println("Strings are equal");
//			
//			System.out.println("OK NEXT PAGE");
//		} else{			
//			System.out.println("ERROR 404 (PLEASE CHANGE MOBILE NUMBER)");
//			driver.quit();
//	}
		}
	

	@And("user write code (.*)$")
	public void user_write_code (String code)  throws InterruptedException{
		
		Thread.sleep(4000);
		   Register = new Register_page(driver);
		   Register.insertCode(code);
		   Thread.sleep(4000);
			Register.click_Subscribe();
			Thread.sleep(4000);	  
	}
	
	@And("user won (.*)$")
	public void user_won (String points) throws InterruptedException {
		
		Thread.sleep(10000);	
		
		WebElement strValue = driver.findElement(By.xpath("//*[@id=\"daily_bonus_points_reward\"]/div[1]/div/div[1]/h1[2]"));
		String strExpected = "You won 100 points!";
		String strActual = strValue.getText();
		System.out.println(strActual);
				
		if (strExpected.equals(strActual )) {
			//System.out.println("Strings are equal");
			driver.findElement(By.xpath("//*[@id=\"daily_bonus_points_reward\"]/div[1]/div/div[2]/div/a")).click();
			//System.out.println("You won 100 points!");
		} else{			
			System.out.println("ERROR 404 (Take wrong points)");
			driver.quit();
			Thread.sleep(10000);	  		
			
	}  		
		Thread.sleep(4000);		
	}
	
	@And("seems the welcome with user")
	public void seems_the_welcome_with_user() throws InterruptedException{
		Thread.sleep(10000);	
		String heading = driver.findElement(By.xpath("//*[@id=\"welcome-sign-up\"]/div[1]/div/div[1]/h1")).getText();        	
		System.out.println("Welcome message is --- "+heading);
		//Thread.sleep(10000);	
		//
		Thread.sleep(10000);	
		
		WebElement strValue = driver.findElement(By.xpath("//*[@id=\"welcome-sign-up\"]/div[1]/div/div[2]"));
		String strExpected = "Proof your knowledge by playing games against others. In every game, you can win points for our prizes!";
		String strActual = strValue.getText();
		System.out.println(strActual);
				
		if (strExpected.equals(strActual )) {
			//System.out.println("Strings are equal");
			driver.findElement(By.className("icon-close")).click();
			//System.out.println("You won 100 points!");
		} else{			
			System.out.println("ERROR 404 (wrong text)");
			driver.quit();
			Thread.sleep(4000);	  		
			
	}  		
		
		Thread.sleep(4000);
		
		
	}

	@Then("user is navigated to the home page")
	public void user_is_navigated_to_the_home_page() throws InterruptedException{
		Thread.sleep(4000);
		String actualTitle = driver.getTitle();
		String expectedTitle ="Quiz and Win | Trivia Fun";
		assertEquals(expectedTitle,actualTitle);
		
		if(driver.getTitle().contains("Quiz and Win | Trivia Fun"))
		 System.out.println("Page title contains Quiz and Win | Trivia Fun");
		
		else
			
			System.out.println("ERROR 500-Page title doesn't contains Quiz and Win | Trivia Fun");
                  
		
	}
	
	
	@And("delete user with (.*)$")
	public void delete_user_with(String password) throws InterruptedException {
	    
		
	Thread.sleep(4000);
		
		data = new JavaMySQLTest(driver);
		data.database();
		Thread.sleep(4000);
	}
	
	
	
	@And("user clicks on Login")
	public void user_clicks_on_login() throws InterruptedException{
		
		
		
		Thread.sleep(4000);
		
		//driver.findElement(By.xpath("//*[@id=\"nav_dropdown\"]/ul/li[4]/a")).click();
		
		driver.findElement(btn_Login).click();
		Thread.sleep(4000);
	}
	
	@When("^user writes (.*) and (.*)$")
	public void user_writes_mobilenumberlogin_and_passwordlogin(String mobilenumberlogin, String passwordlogin) throws InterruptedException{
		
		Thread.sleep(4000);
		Login = new Login_page(driver);
		Login.insertPassword_Login(mobilenumberlogin);
		Login.insertCode_Login(passwordlogin);
		   Thread.sleep(4000);
		   Login.click_Login();
			Thread.sleep(4000);	  
	    
	}

	

		
	}
