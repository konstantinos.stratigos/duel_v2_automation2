package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Browsers {
	
	protected WebDriver driver;	
	
	
	public Browsers(WebDriver driver)
	{
		this.driver=driver;
	}

	public void chrome() {
	//chrome drivers
			String projectPath =System.getProperty("user.dir");
			System.out.println("Project path is : "+ projectPath);
			System.setProperty("webdriver.chrome.driver",projectPath +"/src/test/resources/drivers/chromedriver.exe");
		    driver = new ChromeDriver();
			driver.get("https://staging-duel-bah-batelco_02.velti.com:493/");		
	}
	//firefox drivers
//			String projectPath =System.getProperty("user.dir");
//			System.out.println("Project path is : "+ projectPath);
//			System.setProperty("webdriver.gecko.driver",projectPath +"/src/test/resources/drivers/geckodriver.exe");
//			WebDriver driver = new FirefoxDriver();
//			driver.get("http://staging-duel-bah-batelco_02.velti.com:493/");
	
	
	

}
