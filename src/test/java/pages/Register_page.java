package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Register_page {

	protected WebDriver driver;	

	private By txt_password= By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/form/div[1]/input");
	private By btn_continue =By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/form/input");
	private By txt_code =    By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/form/div[1]/input");
	private By btn_Subscribe=By.xpath("//*[@id=\"app\"]/div/div/div/div/div/div[3]/form/input");

	public  Register_page(WebDriver driver)
	{
		this.driver=driver;
	}

	public void insertPassword(String password) {

		driver.findElement(txt_password).sendKeys(password);	
	}
	
	public void click_continue() {
		driver.findElement(btn_continue).click();
	}	

	public void insertCode(String code) {

		driver.findElement(txt_code).sendKeys(code);

	}
	public void click_Subscribe() {
		driver.findElement(btn_Subscribe).click();
	}	







}
