package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Login_page {
	protected WebDriver driver;	

	private By txt_mobile_Login= By.xpath("//*[@id=\"login\"]/div/div[2]/div/div/form/div[1]/input");
	private By txt_password_Login = By.xpath("//*[@id=\"login\"]/div/div[2]/div/div/form/div[2]/input");
	private By btn_Login=By.xpath("//*[@id=\"login\"]/div/div[2]/div/div/form/input");

	public  Login_page(WebDriver driver)
	{
		this.driver=driver;
	}

	public void insertPassword_Login(String mobilenumberlogin) {

		driver.findElement(txt_mobile_Login).sendKeys(mobilenumberlogin);	
	}
	
	public void insertCode_Login(String passwordlogin) {

		driver.findElement(txt_password_Login).sendKeys(passwordlogin);

	}
	public void click_Login() {
		driver.findElement(btn_Login).click();
	}	


}


