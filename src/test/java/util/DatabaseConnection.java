//package util;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//
//import org.junit.Assert;
//
//
//public class DatabaseConnection extends driverFactory {
//
//    ReadConfigFile config = new ReadConfigFile();
//
//    public static String dbUrl;                 
//    public static String username;  
//    public static String password;
//    public static String database_driver;
//    Connection con;
//    Statement stmt;
//    String query;
//    ResultSet rs;
//
//    public DatabaseConnection() {
//        super();
//    }
//
//    public DatabaseConnection createConnection() throws SQLException, ClassNotFoundException {
//        con = DriverManager.getConnection(config.getUrl("jdbc:mysql:k44db12c-scan.velti.net:1521"),config.getUsername("DUELQA_BAH_BATELCO_02_SDP"),config.getPassword("DUELQA_BAH_BATELCO_02_SDP"));
//        Class.forName(config.getDatabaseDriver("com.mysql.cj.jdbc.Driver"));  
//        return new DatabaseConnection();
//    }
//
//    public DatabaseConnection createQuery() throws SQLException {
//        query = "select * from test where no = 1;";
//        stmt = con.createStatement();   
//        return new DatabaseConnection();
//    }
//
//    public DatabaseConnection executeQuery() throws SQLException {
//        rs = stmt.executeQuery(query);
//        return new DatabaseConnection();
//    }
//
//    public DatabaseConnection assertRecords(String name) throws SQLException {
//        while (rs.next()){
//            String myName = rs.getString(2);                                                                                           
//            Assert.assertEquals(myName,name);
//        }
//        return new DatabaseConnection();
//    }
//
//    public DatabaseConnection closeConnection() throws SQLException {
//        con.close();
//        return new DatabaseConnection();
//    }
//}
